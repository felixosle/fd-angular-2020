import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './auth/login/login.component';
import { DetalleciudadanoComponent } from './ciudadano/detalle-ciudadano/detalle-ciudadano.component';
import { NuevociudadanoComponent } from './ciudadano/nuevo-ciudadano/nuevo-ciudadano.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { BuscaciudadanoComponent } from './ciudadano/busca-ciudadano/busca-ciudadano.component';

const routes: Routes = [
  { path: '', component: NuevociudadanoComponent },
  { path: 'login', component: LoginComponent},
  { path: 'buscaciudadano', component: BuscaciudadanoComponent},
  { path: 'nuevociudadano', component: NuevociudadanoComponent},
  { path: 'ciudadanos/:idciudadano', component: DetalleciudadanoComponent },
  { path: 'logout', component: LogoutComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
