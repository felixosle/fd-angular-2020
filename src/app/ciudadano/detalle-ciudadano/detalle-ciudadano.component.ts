import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogoConfirmacionComponent } from '../dialogo-confirmacion/dialogo-confirmacion.component';
import { DialogoEdicionComponent } from '../dialogo-edicion/dialogo-edicion.component';
import { Validators, NgForm, FormBuilder } from '@angular/forms';
import {ciudadano} from '../../api/model/ciudadano'
import { ciudadanoMockService } from '../ciudadano.mock.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'detalle-ciudadano',
  templateUrl: './detalle-ciudadano.component.html',
  styleUrls: ['./detalle-ciudadano.component.scss']
})
export class DetalleciudadanoComponent implements OnInit {
  idciudadano: number;

  formulariociudadano = this.fb.group({
    id: [null],
    // Deshabilitamos nif porque no queremos permitir que lo cambien alegremente:
    nif: [{value: '', disabled: true}, Validators.required],
    nombre: ['', Validators.required],
    fechaNacimiento: [''],
    sexo:[''],
    pais: [''],
    direccion: this.fb.group({
      tipoVia: [''],
      nombreVia: ['', Validators.required],
      numeroVia: ['', Validators.required],
      bisDup: [''],
      escalera: [''],
      piso: [''],
      letraNumeroPuerta: [''],
      provincia: ['', Validators.required],
      municipio: ['', Validators.required],
      codigoPostal: ['', Validators.required],
      localidad: ['']
    }),
    telefono: ['', Validators.required],    
    email: [''],
    idiomaComunicacion: ['', Validators.required]
  });
  // Aquí se indica el orden en el que aparecen las columnas en el html:
  displayedColumns = ['numero', 'fecha_inicio', 'fecha_fin', 'editar', 'borrar'];
  dataSource = ELEMENT_DATA;
  ciudadanos: ciudadano[];
  ciudadano: ciudadano;
 
  constructor(private dialog: MatDialog, private fb: FormBuilder, private mock: ciudadanoMockService, private route: ActivatedRoute, private router:Router, private snackBar: MatSnackBar) {
   }

  ngOnInit() {
    // Recuperamos el id del ciudadano sacado de la url
    this.idciudadano = this.route.snapshot.params['idciudadano'];
    // Pedimos el ciudadano al servicio
    this.mock.getciudadanoPorID(this.idciudadano).subscribe( 
      data => {
        // Escribimos los datos en el formulario
        this.formulariociudadano.patchValue(data);
        // Como dirección es un subgrupo de ciudadano, tenemos que escribirlo aparte:
        // (Compensa tratarlo aparte porque dirección es un componente que acabaremos separando para reutilizarlo)
        this.formulariociudadano.patchValue({direccion: data});
        console.log('formulariociudadano.value= ');
        console.log(this.formulariociudadano.value);
    });    
  }

  onSubmit(f){    
    this.ciudadano = f.value;
    this.mock.updateciudadano(this.ciudadano).subscribe();
    let snackBarRef = this.snackBar.open('ciudadano: ' + this.formulariociudadano.value.nombre + ' actualizado correctamente', null, {
      duration:5000
    });
    snackBarRef.afterDismissed().subscribe(() => {
      // Acción que queremos hacer al cerrar el snackbar: Refescar, navegar a otro sitio... (en este caso, de momento, nada)
    });
  }
  
  onDelete(){
    console.log("Borrando elemento " + this.formulariociudadano.value.nombre );
    const dialogRef = this.dialog.open(DialogoConfirmacionComponent, {
      // En data pasamos el <NOMBRE> (texto) que se muestra en el diálogo. "¿Seguro que desea borrar el elemento <NOMBRE>?"
      data: {
        nombre: this.formulariociudadano.value.nombre
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log("Confirma el borrado");
        this.mock.deleteciudadano(this.formulariociudadano.value.id).subscribe();
        let snackBarRef = this.snackBar.open('ciudadano: ' + this.formulariociudadano.value.nombre + ' eliminado correctamente', null, {
          duration:6000
        });
        this.router.navigate(['buscaciudadano']);
        snackBarRef.afterDismissed().subscribe(() => {                
        });
        } else {
          console.log("Cancela el borrado");
        }
      });
  }

// TODO: Definir los datos pasados a los diálogos onEdit y onDelete AmbioActuacion
  onEditAmbitoActuacion(id){
    console.log("Editando elemento " + id);
    const dialogRef = this.dialog.open(DialogoEdicionComponent, {
      data: {ambito: 'Local', numero: 1234567, fecha_inicio: '01/01/2017', fecha_fin: '30/01/2017'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log("Sí");
      } else {
        console.log("No");
      }
    });
  }
  
  onDeleteAmbitoActuacion(id){
    console.log("Borrando elemento " + id);
    const dialogRef = this.dialog.open(DialogoConfirmacionComponent, {
      data: {
        id: '1'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log("Sí");
      } else {
        console.log("No");
      }
    });
  }


  // Comprueba si es un DNI correcto (entre 5 y 8 letras seguidas de la letra que corresponda).
  // Acepta NIEs (Extranjeros con X, Y o Z al principio)
  validaDNI(dni) {
    var numero, letr, letra;
    var expresion_regular_dni = /^[XYZ]?\d{5,8}[A-Z]$/;

    dni = dni.toUpperCase();

    if(expresion_regular_dni.test(dni) === true){
        numero = dni.substr(0,dni.length-1);
        numero = numero.replace('X', 0);
        numero = numero.replace('Y', 1);
        numero = numero.replace('Z', 2);
        letr = dni.substr(dni.length-1, 1);
        numero = numero % 23;
        letra = 'TRWAGMYFPDXBNJZSQVHLCKET';
        letra = letra.substring(numero, numero+1);
        if (letra != letr) {
            //alert('Dni erroneo, la letra del NIF no se corresponde');
            return false;
        }else{
            //alert('Dni correcto');
            return true;
        }
    }else{
        //alert('Dni erroneo, formato no válido');
        return false;
      }
  }

  validaCIF(dni){

    var validChars = 'TRWAGMYFPDXBNJZSQVHLCKET';
    var nifRexp = /^[0-9]{8}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    var nieRexp = /^[XYZ]{1}[0-9]{7}[TRWAGMYFPDXBNJZSQVHLCKET]{1}$/i;
    var str = dni.toString().toUpperCase();

    if (!nifRexp.test(str) && !nieRexp.test(str)) return false;

    var nie = str
        .replace(/^[X]/, '0')
        .replace(/^[Y]/, '1')
        .replace(/^[Z]/, '2');

    var letter = str.substr(-1);
    var charIndex = parseInt(nie.substr(0, 8)) % 23;

    if (validChars.charAt(charIndex) === letter) return true;
    
    return false;
  }
}
export interface Solicitud {  
  numero: number;
  fecha_inicio: string;
  fecha_fin: string;
}

const ELEMENT_DATA: Solicitud[] = [
  {numero: 1234567, fecha_inicio: '01/01/2017', fecha_fin: '30/01/2017'},
  {numero: 78734567, fecha_inicio: '01/01/2017', fecha_fin: '19/05/2017'},
  {numero: 56943347, fecha_inicio: '27/01/2017', fecha_fin: '24/08/2017'},
  {numero: 34878945, fecha_inicio: '15/01/2017', fecha_fin: '18/07/2017'},
  {numero: 83964524, fecha_inicio: '18/01/2017', fecha_fin: '13/02/2017'},
  {numero: 2874783, fecha_inicio: '23/01/2017', fecha_fin: '21/12/2017'},
  {numero: 49789387, fecha_inicio: '12/01/2017', fecha_fin: '30/09/2017'}
];