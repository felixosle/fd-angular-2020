import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DetalleciudadanoComponent } from './detalle-ciudadano.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('DetalleCiudadanoComponent', () => {
  let component: DetalleciudadanoComponent;
  let fixture: ComponentFixture<DetalleciudadanoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleciudadanoComponent ],
      // Con este esquema Le indicamos a Angular que ignore las etiquetas desconocidas (por ejemplo si contiene otros componentes):
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleciudadanoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
