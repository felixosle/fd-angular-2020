import { Component, OnInit } from '@angular/core';
import { Validators, AbstractControl, NgForm, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { DialogoConfirmacionComponent } from '../dialogo-confirmacion/dialogo-confirmacion.component';
import { DialogoEdicionComponent } from '../dialogo-edicion/dialogo-edicion.component';
import {ciudadano} from '../../api/model/ciudadano'
import { ciudadanoMockService } from '../ciudadano.mock.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

// Incluimos las clases de validación (Podrían ir a nivel de proyecto, las dejamos aquí por sencillez)
import { PersonalDataValidator } from '../validators/personal-data.validator';
// ToDo: meter una validación de fecha (compleja):
// import { DateValidator } from '../validators/date.validator';

@Component({
  selector: 'nuevo-ciudadano',
  templateUrl: './nuevo-ciudadano.component.html',
  styleUrls: ['./nuevo-ciudadano.component.css']
})

export class NuevociudadanoComponent implements OnInit {
  // Expresión regular para comprobar el formato de NIF (OBSOLETO porque usamos un validador específico)
  // formatoNif ='[X|Y|Z]?[0-9]{5,8}[A-Z]{1}'
  formatoTelefono='[9|6|7][0-9]{8}'
  formatoCodigoPostal='([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}'

  formulariociudadano = this.fb.group({
    nif: ['', 
      [Validators.required,
        PersonalDataValidator.personalIdentificationNumber]
      // Si usáramos el patrón de formatoNif sería así (Obsoleto porque usamos un validador específico):
      // Validators.pattern(this.formatoNif)]
    ],
    nombre: ['', Validators.required],
    fechaNacimiento: [''],
    sexo:[''],
    pais: [''],
    direccion: this.fb.group({
      tipoVia: [''],
      nombreVia: ['', Validators.required],
      numeroVia: ['', Validators.required],
      bisDup: [''],
      escalera: [''],
      piso: [''],
      letraNumeroPuerta: [''],
      provincia: ['', Validators.required],
      municipio: ['', Validators.required],
      codigoPostal: ['', 
        [Validators.required, 
        Validators.pattern(this.formatoCodigoPostal)]
        ],
      localidad: ['']
    }),
    telefono: ['', 
      [Validators.required,
      Validators.pattern(this.formatoTelefono)]
      ],    
    email: [''],
    idiomaComunicacion: ['', Validators.required]    
  });
  // Aquí se indica el orden en el que aparecen las columnas:
  displayedColumns = ['numero', 'fecha_inicio', 'fecha_fin', 'editar', 'borrar'];
  dataSource = ELEMENT_DATA;
  ciudadanos: ciudadano[];
  ciudadanoNuevo: ciudadano;
 
  constructor(private dialog: MatDialog, private fb: FormBuilder, private mock: ciudadanoMockService, private snackBar: MatSnackBar, private router:Router) {
   }

  ngOnInit() {
  }

  onSubmit(f){
    console.log('Valores del formulario: ');
    console.log(f.value);
    console.log('Estado del formulario: ' + f.valid);
    this.ciudadanoNuevo = f.value;
    this.mock.createciudadano(this.ciudadanoNuevo).subscribe(
      // TODO: Es necesario recuperar el id creado para navegar a él
    );
    let snackBarRef = this.snackBar.open('ciudadano: ' + this.formulariociudadano.value.razonSocial + ' creado correctamente', null, {
      duration:5000
    });
    snackBarRef.afterDismissed().subscribe(() => {
      // Acción que queremos hacer al cerrar el snackbar: Refescar, navegar a otro sitio... (en este caso, de momento, nada)
      // this.router.navigate(['ciudadanos/' + ); TODO: Añadir el id del ciudadano recién creado para navegar a él
    });
  }
  
  onEdit(id){
    console.log("Editando elemento " + id);
    const dialogRef = this.dialog.open(DialogoEdicionComponent, {
      data: {numero: 1234567, fecha_inicio: '01/01/2017', fecha_fin: '30/01/2017'}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log("Sí");
      } else {
        console.log("No");
      }
    });
  }

  onDelete(id){
    console.log("Borrando elemento " + id);
    const dialogRef = this.dialog.open(DialogoConfirmacionComponent, {
      data: {
        id: '1'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log("Sí");
      } else {
        console.log("No");
      }
    });
  }

}
export interface Solicitud {  
  numero: number;
  fecha_inicio: string;
  fecha_fin: string;
}

const ELEMENT_DATA: Solicitud[] = [
  {numero: 1234567, fecha_inicio: '01/01/2017', fecha_fin: '30/01/2017'},
  {numero: 78734567, fecha_inicio: '01/01/2017', fecha_fin: '19/05/2017'},
  {numero: 56943347, fecha_inicio: '27/01/2017', fecha_fin: '24/08/2017'},
  {numero: 34878945, fecha_inicio: '15/01/2017', fecha_fin: '18/07/2017'},
  {numero: 83964524, fecha_inicio: '18/01/2017', fecha_fin: '13/02/2017'},
  {numero: 2874783, fecha_inicio: '23/01/2017', fecha_fin: '21/12/2017'},
  {numero: 49789387, fecha_inicio: '12/01/2017', fecha_fin: '30/09/2017'}
];