import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { BrowserModule, By } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

//Imports de Angular Material:
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';

import { NuevociudadanoComponent } from './nuevo-ciudadano.component';
// import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('NuevociudadanoComponent', () => {
  let comp: NuevociudadanoComponent;
  let fixture: ComponentFixture<NuevociudadanoComponent>;
  let de: DebugElement;
  let el: HTMLElement; 

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NuevociudadanoComponent ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterTestingModule,
        MatMomentDateModule,
        //Imports de Angular Material:
        MatButtonModule, MatIconModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, 
        MatSidenavModule, MatToolbarModule, MatListModule, MatTabsModule, MatCardModule, 
        MatSelectModule, MatProgressSpinnerModule, MatDialogModule, MatTableModule, MatSnackBarModule,
        MatCheckboxModule, MatExpansionModule, MatStepperModule, MatMenuModule
      ],
      // schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NuevociudadanoComponent);
    comp = fixture.componentInstance;
    de = fixture.debugElement.query(By.css('form'));
    el = de.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(comp).toBeTruthy();
  });

  it(`El Formulario debería ser inválido al dejar campos oblitagorios vacíos`, async(() => {
    comp.formulariociudadano.controls['nif'].setValue('');
    comp.formulariociudadano.controls['nombre'].setValue('');
    comp.formulariociudadano.controls['telefono'].setValue('');
    expect(comp.formulariociudadano.valid).toBeFalsy();
  }));

  it(`El Formulario debería ser válido rellenando todos los campos obligatorios`, async(() => {
    comp.formulariociudadano.controls['nif'].setValue('12345678Z');
    comp.formulariociudadano.controls['nombre'].setValue('Federico Jiménez Losantos');
    // Los siguientes campos están dentro del grupo dirección y se acceden de forma diferente:
    comp.formulariociudadano.get('direccion.nombreVia').setValue('CL');
    comp.formulariociudadano.get('direccion.numeroVia').setValue('4');
    comp.formulariociudadano.get('direccion.provincia').setValue('VALLADOLID');
    comp.formulariociudadano.get('direccion.municipio').setValue('VALLADOLID');
    comp.formulariociudadano.get('direccion.codigoPostal').setValue('47001');
    comp.formulariociudadano.controls['telefono'].setValue('987654321');
    comp.formulariociudadano.controls['idiomaComunicacion'].setValue('Español');
    expect(comp.formulariociudadano.valid).toBeTruthy();
  }));

});
