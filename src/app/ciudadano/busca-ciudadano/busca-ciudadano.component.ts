import { Component, OnInit, ViewChild } from '@angular/core';
import { ciudadano } from 'src/app/api/model/ciudadano';
import { NgForm, FormBuilder } from '@angular/forms';
import { ciudadanoMockService } from '../ciudadano.mock.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-busca-ciudadano',
  templateUrl: './busca-ciudadano.component.html',
  styleUrls: ['./busca-ciudadano.component.css']
})
export class BuscaciudadanoComponent implements OnInit{
// Fuentes de mock service con json-server:
// https://blog.angulartraining.com/fake-your-angular-backend-until-you-make-it-8d145f713e14
// https://www.djamware.com/post/5da31946ae418d042e1aef1d/angular-8-tutorial-observable-and-rxjs-examples
// https://www.djamware.com/post/5d8d7fc10daa6c77eed3b2f2/angular-8-tutorial-rest-api-and-httpclient-examples
  
displayedColumns: string[] = ['editar', 'nif', 'nombre'];
dataSource = new MatTableDataSource<ciudadano>();
resultsLength = 0;
isLoadingResults = false;

@ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
@ViewChild(MatSort, {static: true}) sort: MatSort;

ngOnInit() {
  this.dataSource.paginator = this.paginator;
  this.dataSource.sort = this.sort;
}

ciudadanos: ciudadano[] = [];
  public dni: string="";
  formularioBusqueda = this.fb.group({
    nif: [''],
    nombre: ['']
  });

  constructor(private mock: ciudadanoMockService, private fb: FormBuilder) {
  }

  onSubmit(f) {
    console.log('Estado del formulario: ' + f.valid);
    this.isLoadingResults = true;
    this.mock.getciudadanos(this.formularioBusqueda.value.nif, this.formularioBusqueda.value.nombre).subscribe(
      respuesta => {
        this.dataSource.data = respuesta;
        this.resultsLength = respuesta.length;
        this.isLoadingResults = false;
      }
    )
  }
}
