import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ciudadanoMockService } from './ciudadano.mock.service';

describe('Empresario.MockService', () => {
  let mock: ciudadanoMockService;
  beforeEach(async(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule,
    ],
    providers: [
      ciudadanoMockService
    ],   
  })));

  it('should be created', () => {
    const service: ciudadanoMockService = TestBed.get(ciudadanoMockService);
    expect(service).toBeTruthy();
  });

  // it('Debería recibir ciudadanos de la búsqueda', async(() => {    
  //   mock.getciudadanos().subscribe((respuesta: any) => {
  //     expect(respuesta.length).toBeGreaterThan(1);
  //   });
  // }));

});