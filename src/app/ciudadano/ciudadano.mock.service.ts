// Fuentes:
// Fake Your Angular Backend Until You Make It:
// https://blog.angulartraining.com/fake-your-angular-backend-until-you-make-it-8d145f713e14

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ciudadano } from 'src/app/api/model/ciudadano';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class ciudadanoMockService {
  url: string = '/api/ciudadanos';
  
  constructor(private http: HttpClient) {};  

  getciudadanos( nif?: String, caf?: String, razonSocial?: String) {
    // Inicializamos la url para limpiarla:
    this.url = '/api/ciudadanos';
    if (nif) {
      console.log ('Entrado en nif != null, valor de nif: ' + nif);
      this.url = this.url + '?nif_like=' + nif;
      console.log (this.url);
    };    
    if (razonSocial) {
      if (nif || caf) this.url = this.url + '&';
      this.url = this.url + '?razonSocial_like=' + razonSocial;
      console.log (this.url);
    };
    return this.http.get<ciudadano[]>(this.url);
   }
   
   getciudadanoPorID(idciudadano:number){
    this.url = '/api/ciudadanos/' + idciudadano;
    return this.http.get<ciudadano>(this.url);
   }

   createciudadano(ciudadano: ciudadano): Observable<ciudadano>{
    // Inicializamos la url para limpiarla:
    this.url = '/api/ciudadanos';
    return this.http.post<ciudadano>(this.url, ciudadano, httpOptions);
   }

   updateciudadano(ciudadano: ciudadano): Observable<ciudadano>{
    // Inicializamos la url para limpiarla:
    this.url = '/api/ciudadanos/' + ciudadano.id;    
    return this.http.put<ciudadano>(this.url, ciudadano, httpOptions);
   }

   deleteciudadano(ciudadanoId:number){
    this.url = '/api/ciudadanos/' + ciudadanoId;
    return this.http.delete(this.url);
   }
}