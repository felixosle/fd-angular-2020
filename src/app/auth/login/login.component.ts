import { Component, OnInit } from '@angular/core';
// Usamos un FormGroup que contiene FormControls:
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  loginForm = new FormGroup({
    usuario: new FormControl(''),
    password: new FormControl(''),
  })
  
  constructor() { }

  ngOnInit() {
  }

  onSubmit(){    
  }
}