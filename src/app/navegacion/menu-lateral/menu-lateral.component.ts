import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { NavItem } from '../../api/model/nav-item';
import { menuMockService } from './menu.mock.service';

@Component({
  selector: 'app-menu-lateral',
  templateUrl: './menu-lateral.component.html',
  styleUrls: ['./menu-lateral.component.css']
})
export class MenuLateralComponent implements OnInit {
  @Output() closeSidenav = new EventEmitter<void>();  
  menu: NavItem[] = null;

  // ToDo: ID PERFIL FIJO A 1, PROGRAMAR SELECCION DE PERFIL
  
  idPerfil: number = null;  
  constructor(private mock: menuMockService) { }

  
  ngOnInit() {
    // Pedimos el menu al servicio
    if (!localStorage.getItem('perfilID'))
    {
      this.idPerfil = 1;
    }
    else this.idPerfil= parseInt(localStorage.getItem('perfilID'));

    this.mock.getPerfilPorID(this.idPerfil).subscribe( 
      data => {
        // Escribimos los datos en el menu
        this.menu = data.menu;        
        console.log('perfil de ' + data.tipo + ', id=' + data.id);
        // Guardamos el perfil en almacenamiento local del navegador
        localStorage.setItem('perfilID', data.id.toString());
        localStorage.setItem('perfilTipo', data.tipo);
        console.log(this.menu);
    });
  }

  onClose() {
    //este evento lo recibirá dinámicamente app.component.html en (closeSidenav)="sidenav.close()"
    this.closeSidenav.emit();    
  }

}
