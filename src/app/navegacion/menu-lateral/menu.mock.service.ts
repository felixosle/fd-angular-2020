// Fuentes:
// Fake Your Angular Backend Until You Make It:
// https://blog.angulartraining.com/fake-your-angular-backend-until-you-make-it-8d145f713e14

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NavItem, Perfil } from '../../api/model/nav-item';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class menuMockService {
  url: string = '/api/menu';
  perfil: Perfil = null;
  
  constructor(private http: HttpClient) {};  

  getmenus( nif?: String, caf?: String, razonSocial?: String) {
    // Inicializamos la url para limpiarla:
    this.url = '/api/perfiles';
    if (nif) {
      console.log ('Entrado en nif != null, valor de nif: ' + nif);
      this.url = this.url + '?nif_like=' + nif;
      console.log (this.url);
    };    
    if (razonSocial) {
      if (nif || caf) this.url = this.url + '&';
      this.url = this.url + '?razonSocial_like=' + razonSocial;
      console.log (this.url);
    };
    return this.http.get<NavItem[]>(this.url);
   }
   
   getPerfilPorID(idmenu:number){
    this.url = '/api/perfiles/' + idmenu;
    return this.http.get<Perfil>(this.url);
   }

   createmenu(menu: NavItem): Observable<NavItem>{
    // Inicializamos la url para limpiarla:
    this.url = '/api/menus';
    return this.http.post<NavItem>(this.url, menu, httpOptions);
   }

  //  updatemenu(menu: NavItem): Observable<NavItem>{
  //   // Inicializamos la url para limpiarla:
  //   this.url = '/api/menus/' + menu.id;    
  //   return this.http.put<NavItem>(this.url, menu, httpOptions);
  //  }

   deletemenu(menuId:number){
    this.url = '/api/menus/' + menuId;
    return this.http.delete(this.url);
   }
}