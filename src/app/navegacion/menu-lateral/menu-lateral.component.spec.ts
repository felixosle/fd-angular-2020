// Fuentes: https://medium.com/bb-tutorials-and-thoughts/angular-a-comprehensive-guide-to-unit-testing-with-angular-and-best-practices-e1f9ef752e4e
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MenuLateralComponent } from './menu-lateral.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('MenuLateralComponent', () => {
  let component: MenuLateralComponent;
  let fixture: ComponentFixture<MenuLateralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MenuLateralComponent ],
      // Con este esquema Le indicamos a Angular que ignore las etiquetas desconocidas (por ejemplo si contiene otros componentes):
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuLateralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
