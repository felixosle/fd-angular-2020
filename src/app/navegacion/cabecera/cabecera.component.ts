import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-cabecera',
  templateUrl: './cabecera.component.html',
  styleUrls: ['./cabecera.component.css']
})
export class CabeceraComponent implements OnInit {
  @Output() sidenavToggle = new EventEmitter<void>();

  perfilID: string= null;
  perfilTipo: string = null;
  
  constructor() { }

  ngOnInit() {
    this.perfilID = localStorage.getItem('perfilID');
    this.perfilTipo = localStorage.getItem('perfilTipo');
  }

  onToggleSidenav() {
    this.sidenavToggle.emit();
    //este evento lo recibirá dinámicamente app.component.html en (sidenavToggle)="sidenav.toggle()"
  }

  logout(){
    console.log("Pulsado botón Salir");
  }

  onClick(){
    localStorage.setItem('perfilID', this.perfilID);
    if (this.perfilID == "1") this.perfilTipo="usuario";
    if (this.perfilID == "2") this.perfilTipo="gestor";
    if (this.perfilID == "3") this.perfilTipo="administrador";

    console.log("perfil: " + this.perfilTipo);
    localStorage.setItem('perfilTipo', this.perfilTipo);

    alert('Perfil cambiado a ' + localStorage.getItem('perfilID'));
     // Refrescamos toda la página para cargar el nuevo perfil:
     window.location.reload();
     // ToDo: lo suyo sería refrescar sólo el menú lateral, pero con
     // this.menuComp.ngOnInit();
     // no se consigue refrescar la vista (html)
  }
}
