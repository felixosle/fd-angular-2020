export interface NavItem {
  displayName: string;
  disabled?: boolean;
  iconName: string;
  route?: string;
  children?: NavItem[];
}

export interface Perfil{
  id: number;
  tipo: string;
  menu: NavItem[];
}