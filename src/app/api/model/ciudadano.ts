export interface ciudadano {
    id: number; /* NIF del ciudadano (NUMBER) */
    
    nif: string; /* NIF del ciudadano (9 CHAR) */

    nombre: string; /* NOMBRE O RAZON SOCIAL (55 CHAR) */
    
    fechaNacimiento?: Date; /* FECHA DE NACIMIENTO */

    sexo: string; /* SEXO */
    
    pais?: string; /* PAÍS DE PROCEDENCIA (3 CHAR) */

    tipoVia: string; /* TIPO VIA PUBLICA (2 CHAR) */
    
    nombreVia: string; /* NOMBRE VIA PUBLICA (50 CHAR) */
    
    numeroVia: string; /* NUMERO EN VIA PUBLICA (4 CHAR) */
    
    bisDup?: string; /* BIS/DUPLICADO (2 CHAR) */
    
    escalera?: string; /* ESCALERA (2 CHAR) */
    
    piso?: string; /* PISO (2 CHAR) */
    
    letraNumeroPuerta?: string; /* LETRA/Nº PUERTA (3 CHAR) */
    
    municipio: string; /* MUNICIPIO (5 CHAR) */
    
    codigoPostal: string; /* CODIGO POSTAL (5 CHAR) */
    
    localidad?: string; /* LOCALIDAD (7 CHAR) */
    
    telefono?: string; /* TELEFONO CONTACTO (15 CHAR) */
    
    email?: string; /* CORREO ELECTRONICO (70 CHAR) */
}