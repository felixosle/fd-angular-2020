import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';

//Imports de componentes propios:
import { AuthComponent } from './auth/auth.component';
import { LoginComponent } from './auth/login/login.component';
import { CabeceraComponent } from './navegacion/cabecera/cabecera.component';
import { MenuLateralComponent } from './navegacion/menu-lateral/menu-lateral.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { NuevociudadanoComponent } from './ciudadano/nuevo-ciudadano/nuevo-ciudadano.component';
import { DialogoConfirmacionComponent } from './ciudadano/dialogo-confirmacion/dialogo-confirmacion.component';
import { DialogoEdicionComponent } from './ciudadano/dialogo-edicion/dialogo-edicion.component';
import { BuscaciudadanoComponent } from './ciudadano/busca-ciudadano/busca-ciudadano.component';

//Imports de Angular Material:
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';

//Imports relacionados con moment.js:
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter, MatMomentDateModule} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { DetalleciudadanoComponent } from './ciudadano/detalle-ciudadano/detalle-ciudadano.component';

@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    LoginComponent,
    CabeceraComponent,
    MenuLateralComponent,
    LogoutComponent,
    NuevociudadanoComponent,
    DialogoConfirmacionComponent,
    DialogoEdicionComponent,
    BuscaciudadanoComponent,
    DetalleciudadanoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatPaginatorModule,
    MatSortModule,
    //Imports de Angular Material:
    MatButtonModule, MatIconModule, MatFormFieldModule, MatInputModule, MatDatepickerModule, 
    MatSidenavModule, MatToolbarModule, MatListModule, MatTabsModule, MatCardModule, 
    MatSelectModule, MatProgressSpinnerModule, MatDialogModule, MatTableModule, MatSnackBarModule,
    MatCheckboxModule, MatExpansionModule, MatStepperModule, MatMenuModule, MatMomentDateModule
  ],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'},    
  ],
  bootstrap: [AppComponent],
  entryComponents: [DialogoConfirmacionComponent,DialogoEdicionComponent]
})
export class AppModule { }
