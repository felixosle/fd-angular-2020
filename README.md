# FD ANGULAR 2020

Curso de Desarrollo FrontEnd Angular 2020

Este proyecto ha sido generado con [Angular CLI](https://github.com/angular/angular-cli) version 8.3.5.

## Servidores de desarrollo (ng serve y json-server)

Se recomienda ejecutar ambos servidores en dos ventanas de Visual Studio nada más cargar el proyecto

Ejecuta `ng serve` para el servidor de desarrollo. Navega a `http://localhost:4200/`. La aplicación se recargará automáticamente cada vez que guardes cambios en el código. 

Ejecuta `npm run api` para el servidor mock de backend json-server. Navega a `http://localhost:3000`. Permite usar durante el desarrollo llamadas a métodos http (get, post, put, patch, delete y options)

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
